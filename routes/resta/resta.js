module.exports = (express, app) => {
  app.post("/sub", function (req, res) {
    const { number1, number2 } = req.body;

    if (!Number.isInteger(number1)) {
      return res.json({
        message: "El primero numero no es entero",
      });
    }
    if (!Number.isInteger(number2)) {
      return res.json({
        message: "El segundo numero no es entero",
      });
    }

    res.json({
      message: "success",
      result: number1 - number2,
    });
  });
};
