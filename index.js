const express = require("express");
const morgan = require("morgan");
const glob = require("./routes-config.js");

const app = express();
const PORT = 3000;

app.use(morgan("dev"));
app.use(express.json());

glob(express, app);

app.listen(PORT, () => {
  console.log("Server on port 3000");
});
