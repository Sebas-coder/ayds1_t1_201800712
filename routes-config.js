module.exports = (express, app) => {
  const glob = require("glob");
  const path = require("path");

  glob.sync("./routes/**/*.js").forEach((file) => {
    require(path.resolve(file))(express, app);
  });
};
